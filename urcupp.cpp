#include <memory>
#include <urcu.h>

#include "urcupp.h"

namespace
{
  struct init
  {
    init() noexcept { ::rcu_init(); }
    ~init() { ::rcu_barrier(); }
  } init_v;
}

void ctp::rcu_register_thread() noexcept
{
  static thread_local struct thread_init
  {
    thread_init() noexcept { ::rcu_register_thread_memb(); }
    ~thread_init() { ::rcu_unregister_thread(); }
  } thread_init_v;

  static_cast<void>(thread_init_v);
}
