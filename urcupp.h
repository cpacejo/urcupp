#ifndef CTP_URCUPP_H
#define CTP_URCUPP_H

#include <atomic>
#include <functional>
#include <future>
#include <memory>
#include <optional>
#include <tuple>
#include <type_traits>
#include <utility>
#include <urcu.h>

namespace ctp
{
  // If you publicly inherit from this, the managed pointers clean up
  // slightly more efficiently (since they don't have to allocate an rcu_head).
  using ::rcu_head;

  // Note that threads are automatically deregistered.
  #undef rcu_register_thread
  void rcu_register_thread() noexcept;

  inline void synchronize_rcu() noexcept { ::synchronize_rcu(); }

  namespace rcu_internal
  {
    template<typename F, typename... Args>
    struct handle: ::rcu_head
    {
      F f;
      std::tuple<Args...> args;
      template<typename G> handle(G&& f, Args&&... args) noexcept
        : f(std::forward<G>(f)), args(std::forward<Args>(args)...) { }
      static void callback(::rcu_head* const head) noexcept
      {
        std::unique_ptr<handle> h(static_cast<handle*>(head));
        // TODO: not sure if this behaves correctly w/ lvalue ref args
        std::apply(std::move(h->f), std::move(h->args));
      }
    };
  }

  #undef call_rcu
  template<typename F, typename... Args>
  inline void call_rcu_with_args(F&& f, Args&&... args)
  {
    ::call_rcu_memb(static_cast<::rcu_head*>(new rcu_internal::handle<F, Args...>(std::forward<F>(f), std::forward<Args>(args)...)),
      rcu_internal::handle<F, Args...>::callback);
  }

  template<typename R, typename F, typename... Args>
  inline std::future<R> async_rcu_with_args(F&& f, Args&&... args)
  {
    rcu_internal::handle<std::packaged_task<R(Args...)>, Args...>* const h =
      new rcu_internal::handle<std::packaged_task<R(Args...)>, Args...>(std::forward<F>(f), std::forward<Args>(args)...);
    const std::future<R> ret = h->f.get_future();
    ::call_rcu_memb(static_cast<::rcu_head*>(h),
      rcu_internal::handle<std::packaged_task<R(Args...)>, Args...>::callback);
    return ret;
  }

  template<typename F>
  inline void call_rcu(F&& f) { call_rcu_with_args(std::forward<F>(f)); }
  template<typename R, typename F>
  inline std::future<R> async_rcu(F&& f) { return async_rcu_with_args(std::forward<F>(f)); }

  // TODO: defer_rcu?

  inline void rcu_barrier() noexcept { ::rcu_barrier(); }

  struct rcu_read_t
  {
    void lock() const noexcept { ::rcu_read_lock(); }
    void unlock() const noexcept { ::rcu_read_unlock(); }
  };
  inline constexpr rcu_read_t rcu_read{};


  template<typename T>
  class rcu_synchronized_ptr
  {
  public:
    using pointer = T*;

    rcu_synchronized_ptr() noexcept = default;
    rcu_synchronized_ptr(std::nullptr_t) noexcept { }
    explicit rcu_synchronized_ptr(T* const p) noexcept: m_p(p) { }

    rcu_synchronized_ptr(rcu_synchronized_ptr&& other) noexcept
      : m_p(other.m_p) { other.m_p = nullptr; }

    explicit operator bool() const noexcept { return static_cast<bool>(m_p); }

    pointer get() && noexcept
    { if (m_p) { synchronize_rcu(); return release(); } else return nullptr; }

    pointer operator->() && noexcept { return std::move(*this).get(); }
    operator pointer() && noexcept { return std::move(*this).get(); }

    operator std::future<pointer>() &&
    {
      if (m_p)
      {
        std::future<pointer> ret = async_rcu([p = m_p]() { return p; });
        m_p = nullptr;
        return ret;
      }
      else
      {
        std::promise<pointer> p;
        p.set_value(nullptr);
        return p.get_future();
      }
    }

    void reset(std::nullptr_t = nullptr) noexcept { m_p = nullptr; }
    void reset(T* const p) noexcept { m_p = p; }

    pointer release() noexcept { pointer ret(m_p); m_p = nullptr; return ret; }

    rcu_synchronized_ptr& operator=(std::nullptr_t) noexcept { reset(); return *this; }
    rcu_synchronized_ptr& operator=(rcu_synchronized_ptr&& other) noexcept
    { if (this != &other) { m_p = other.m_p; other.m_p = nullptr; } return *this; }

    void swap(rcu_synchronized_ptr& other) noexcept
    { using std::swap; swap(m_p, other.m_p); }

    template<typename F>
    friend inline void call_rcu(rcu_synchronized_ptr&& p, F&& f)
    {
      // NOTE: don't move() or .release() p; call_rcu might throw (it calls new)
      if (p.m_p) { call_rcu_with_args(std::forward<F>(f), p.m_p); p.m_p = nullptr; }
      else std::forward<F>(f)(nullptr);
    }

    template<typename R, typename F>
    friend inline std::future<R> async_rcu(rcu_synchronized_ptr&& p, F&& f)
    {
      if (p.m_p)
      {
        std::future<R> ret = async_rcu_with_args(std::forward<F>(f), p.m_p);
        p.m_p = nullptr;
        return ret;
      }
      else
      {
        std::promise<R> prom;
        prom.set_value(std::forward<F>(f)(nullptr));
        return prom.get_future();
      }
    }

  protected:
    T* m_p = nullptr;
  };

  template<typename T>
  inline void swap(rcu_synchronized_ptr<T>& lhs, rcu_synchronized_ptr<T>& rhs) noexcept
  { lhs.swap(rhs); }

  template<typename T>
  inline bool operator==(const rcu_synchronized_ptr<T>& p, std::nullptr_t) noexcept { return !p; }

  template<typename T>
  inline bool operator==(std::nullptr_t, const rcu_synchronized_ptr<T>& p) noexcept { return !p; }

  template<typename T>
  inline bool operator!=(const rcu_synchronized_ptr<T>& p, std::nullptr_t) noexcept { return static_cast<bool>(p); }

  template<typename T>
  inline bool operator!=(std::nullptr_t, const rcu_synchronized_ptr<T>& p) noexcept { return static_cast<bool>(p); }


  template<typename T>
  class rcu_unmanaged_ptr
  {
  public:
    using pointer = T*;
    using element_type = T;

    rcu_unmanaged_ptr() noexcept = default;
    rcu_unmanaged_ptr(std::nullptr_t) noexcept: m_p(nullptr) { }
    explicit rcu_unmanaged_ptr(const pointer p) noexcept: m_p(p) { }

    explicit operator bool() const noexcept { return static_cast<bool>(get()); }

    pointer get() const noexcept { return m_p.load(std::memory_order_consume); }
    typename std::add_lvalue_reference<T>::type operator*() const { return *get(); }
    pointer operator->() const noexcept { return get(); }

    rcu_unmanaged_ptr& operator=(std::nullptr_t) noexcept { reset(); return *this; }

    rcu_unmanaged_ptr& operator=(const pointer p) noexcept
    { m_p.store(p, std::memory_order_release); return *this; }

    void reset(std::nullptr_t = nullptr) noexcept
    { m_p.store(nullptr, std::memory_order_release); }

    rcu_synchronized_ptr<T> exchange(std::nullptr_t) noexcept
    { return rcu_synchronized_ptr<T>(m_p.exchange(nullptr, std::memory_order_acq_rel)); }

    rcu_synchronized_ptr<T> exchange(const pointer p) noexcept
    { return rcu_synchronized_ptr<T>(m_p.exchange(p, std::memory_order_acq_rel)); }

    // TODO: should expected be const T* instead?
    std::optional<rcu_synchronized_ptr<T>>
    compare_exchange_weak(pointer& expected, std::nullptr_t) noexcept
    {
      if (m_p.compare_exchange_weak(expected, nullptr,
          std::memory_order_acq_rel, std::memory_order_consume))
        return rcu_synchronized_ptr<T>(expected);
      else return std::nullopt;
    }

    std::optional<rcu_synchronized_ptr<T>>
    compare_exchange_weak(pointer& expected, const pointer p) noexcept
    {
      if (m_p.compare_exchange_weak(expected, p,
          std::memory_order_acq_rel, std::memory_order_consume))
        return rcu_synchronized_ptr<T>(expected);
      else return std::nullopt;
    }

    std::optional<rcu_synchronized_ptr<T>>
    compare_exchange_strong(pointer& expected, std::nullptr_t) noexcept
    {
      if (m_p.compare_exchange_strong(expected, nullptr,
          std::memory_order_acq_rel, std::memory_order_consume))
        return rcu_synchronized_ptr<T>(expected);
      else return std::nullopt;
    }

    std::optional<rcu_synchronized_ptr<T>>
    compare_exchange_strong(pointer& expected, const pointer p) noexcept
    {
      if (m_p.compare_exchange_strong(expected, p,
          std::memory_order_acq_rel, std::memory_order_consume))
        return rcu_synchronized_ptr<T>(expected);
      else return std::nullopt;
    }

  protected:
    std::atomic<pointer> m_p;
  };

  template<typename T>
  inline bool operator==(const rcu_unmanaged_ptr<T>& p, std::nullptr_t) noexcept { return !p; }

  template<typename T>
  inline bool operator==(std::nullptr_t, const rcu_unmanaged_ptr<T>& p) noexcept { return !p; }

  template<typename T>
  inline bool operator!=(const rcu_unmanaged_ptr<T>& p, std::nullptr_t) noexcept { return static_cast<bool>(p); }

  template<typename T>
  inline bool operator!=(std::nullptr_t, const rcu_unmanaged_ptr<T>& p) noexcept { return static_cast<bool>(p); }


  // This represents a temporary unique_ptr which has not yet been
  // RCU-synchronized.  All operations on it, except release(), first
  // RCU-synchronize before permitting access.  (Notably, an
  // RCU-synchronization is performed before a delete.)
  template<typename T>
  class rcu_synchronized_unique_ptr
  {
  public:
    using pointer = std::unique_ptr<T>;  // TODO: is this what's expected?

    rcu_synchronized_unique_ptr() noexcept = default;
    rcu_synchronized_unique_ptr(std::nullptr_t) noexcept { }
    explicit rcu_synchronized_unique_ptr(rcu_synchronized_ptr<T>&& p) noexcept
      : m_p(p.release()) { }
    explicit rcu_synchronized_unique_ptr(T* const p) noexcept: m_p(p) { }

    rcu_synchronized_unique_ptr(rcu_synchronized_unique_ptr&& other) noexcept
      : m_p(other.m_p) { other.m_p = nullptr; }

    ~rcu_synchronized_unique_ptr() { reset(); }

    explicit operator bool() const noexcept { return static_cast<bool>(m_p); }

    pointer get() && noexcept
    { if (m_p) { synchronize_rcu(); return release(); } else return nullptr; }

    pointer operator->() && noexcept { return std::move(*this).get(); }
    operator pointer() && noexcept { return std::move(*this).get(); }

    operator std::future<pointer>() &&
    {
      if (m_p)
      {
        std::future<pointer> ret = async_rcu([p = m_p]() { return pointer(p); });
        m_p = nullptr;
        return ret;
      }
      else
      {
        std::promise<pointer> p;
        p.set_value(nullptr);
        return p.get_future();
      }
    }

    void reset(std::nullptr_t = nullptr) noexcept
    {
      if (m_p)
      {
        if constexpr (std::is_base_of_v<::rcu_head, T>)
        {
          struct deleter
          {
            // FIXME: I think this is ill-formed if ::rcu_head is a
            // "base of a virtual base" of T?
            static void callback(::rcu_head* const head) noexcept
            { delete static_cast<T*>(head); }
          };
          ::call_rcu_memb(static_cast<::rcu_head*>(m_p), deleter::callback);
        }
        else call_rcu([p = m_p]() { delete p; });  // FIXME: may terminate if call_rcu throws!
        m_p = nullptr;
      }
    }

    void reset(T* const p) noexcept { reset(); m_p = p; }

    pointer release() noexcept { pointer ret(m_p); m_p = nullptr; return ret; }

    rcu_synchronized_unique_ptr& operator=(std::nullptr_t) noexcept { reset(); return *this; }
    rcu_synchronized_unique_ptr& operator=(rcu_synchronized_unique_ptr&& other) noexcept
    { if (this != &other) { reset(other.m_p); other.m_p = nullptr; } return *this; }

    void swap(rcu_synchronized_unique_ptr& other) noexcept
    { using std::swap; swap(m_p, other.m_p); }

    template<typename F>
    friend inline void call_rcu(rcu_synchronized_unique_ptr&& p, F&& f)
    {
      if (p.m_p)
      {
        call_rcu_with_args(std::forward<F>(f), pointer(p.m_p));
        p.m_p = nullptr;
      }
      else std::forward<F>(f)(pointer());
    }

    template<typename R, typename F>
    friend inline std::future<R> async_rcu(rcu_synchronized_unique_ptr&& p, F&& f)
    {
      if (p.m_p)
      {
        std::future<R> ret = async_rcu_with_args(std::forward<F>(f), pointer(p.m_p));
        p.m_p = nullptr;
        return ret;
      }
      else
      {
        std::promise<R> prom;
        prom.set_value(std::forward<F>(f)(pointer()));
        return prom.get_future();
      }
    }

  protected:
    T* m_p = nullptr;
  };

  template<typename T>
  inline void swap(rcu_synchronized_unique_ptr<T>& lhs, rcu_synchronized_unique_ptr<T>& rhs) noexcept { lhs.swap(rhs); }

  template<typename T>
  inline bool operator==(const rcu_synchronized_unique_ptr<T>& p, std::nullptr_t) noexcept { return !p; }

  template<typename T>
  inline bool operator==(std::nullptr_t, const rcu_synchronized_unique_ptr<T>& p) noexcept { return !p; }

  template<typename T>
  inline bool operator!=(const rcu_synchronized_unique_ptr<T>& p, std::nullptr_t) noexcept { return static_cast<bool>(p); }

  template<typename T>
  inline bool operator!=(std::nullptr_t, const rcu_synchronized_unique_ptr<T>& p) noexcept { return static_cast<bool>(p); }


  // This represents a managed pointer which is part of a concurrent data
  // structure and is to be accessed using RCU.  Any time a pointer is
  // removed from this structure, it is represented as an
  // rcu_synchronized_unique_ptr, so that it is properly RCU-synchronized
  // before use.
  template<typename T>
  class rcu_managed_ptr
  {
  public:
    using pointer = T*;
    using element_type = T;

    rcu_managed_ptr() noexcept = default;
    rcu_managed_ptr(std::nullptr_t) noexcept: m_p(nullptr) { }
    explicit rcu_managed_ptr(std::unique_ptr<T>&& p) noexcept: m_p(p.release()) { }

    explicit operator bool() const noexcept { return static_cast<bool>(m_p); }

    // const T*?
    pointer get() const noexcept { return m_p.get(); }
    typename std::add_lvalue_reference<T>::type operator*() const { return *m_p; }
    pointer operator->() const noexcept { return m_p.operator->(); }

    rcu_managed_ptr& operator=(std::nullptr_t) noexcept
    { reset(); return *this; }

    rcu_managed_ptr& operator=(std::unique_ptr<T>&& p) noexcept
    { exchange(std::move(p)); return *this; }

    void reset(std::nullptr_t = nullptr) noexcept { exchange(nullptr); }

    rcu_synchronized_unique_ptr<T> exchange(std::nullptr_t) noexcept
    { return rcu_synchronized_unique_ptr<T>(m_p.exchange(nullptr)); }

    rcu_synchronized_unique_ptr<T> exchange(std::unique_ptr<T>&& p) noexcept
    { return rcu_synchronized_unique_ptr<T>(m_p.exchange(p.release())); }

    // TODO: should expected be const T* instead?
    std::optional<rcu_synchronized_unique_ptr<T>>
    compare_exchange_weak(pointer& expected, std::nullptr_t) noexcept
    {
      auto ret(m_p.compare_exchange_weak(expected, nullptr));
      if (ret) return std::optional(rcu_synchronized_unique_ptr<T>(std::move(*ret)));
      else return std::nullopt;
    }

    std::optional<rcu_synchronized_unique_ptr<T>>
    compare_exchange_weak(pointer& expected, std::unique_ptr<T>&& p) noexcept
    {
      auto ret(m_p.compare_exchange_weak(expected, p.get()));
      if (ret) { p.release(); return std::optional(rcu_synchronized_unique_ptr<T>(std::move(*ret))); }
      else return std::nullopt;
    }

    std::optional<rcu_synchronized_unique_ptr<T>>
    compare_exchange_strong(pointer& expected, std::nullptr_t) noexcept
    {
      auto ret(m_p.compare_exchange_strong(expected, nullptr));
      if (ret) return std::optional(rcu_synchronized_unique_ptr<T>(std::move(*ret)));
      else return std::nullopt;
    }

    std::optional<rcu_synchronized_unique_ptr<T>>
    compare_exchange_strong(pointer& expected, std::unique_ptr<T>&& p) noexcept
    {
      auto ret(m_p.compare_exchange_strong(expected, p.get()));
      if (ret) { p.release(); return std::optional(rcu_synchronized_unique_ptr<T>(std::move(*ret))); }
      else return std::nullopt;
    }

  protected:
    rcu_unmanaged_ptr<T> m_p;
  };

  template<typename T>
  inline bool operator==(const rcu_managed_ptr<T>& p, std::nullptr_t) noexcept { return !p; }

  template<typename T>
  inline bool operator==(std::nullptr_t, const rcu_managed_ptr<T>& p) noexcept { return !p; }

  template<typename T>
  inline bool operator!=(const rcu_managed_ptr<T>& p, std::nullptr_t) noexcept { return static_cast<bool>(p); }

  template<typename T>
  inline bool operator!=(std::nullptr_t, const rcu_managed_ptr<T>& p) noexcept { return static_cast<bool>(p); }
}

#endif
